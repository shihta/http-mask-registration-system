/*
 * Main.cpp
 *
 *  Created on: Mar 19, 2020
 *      Author: bill
 */

#include <Sys/Include/SysDef.hpp>
#include <Sys/Include/Database.hpp>
#include <drogon/HttpController.h>



int main(int argc,char *argv[]) {
	SYS::Initialize();
	DB::Initialize(filesystem::path(SYS::PATH_DATABASE));

	drogon::app().addListener("0.0.0.0", SYS::HTTP_Port_NO);
	drogon::app().setIdleConnectionTimeout(chrono::microseconds(1500));
	drogon::app().setThreadNum(SYS::HTTP_NUM_THREADS);
	drogon::app().registerHandler("/{string data}",
		[](const drogon::HttpRequestPtr &req,function<void(const drogon::HttpResponsePtr &)> &&callback,string const &data) {
			_STRING DeocdedData( SYS::Base64_Decode(data) );
			_OBJ<IMem> Mem = _MEM::New(DeocdedData.length(),(void *)DeocdedData.c_str());
			SYS::MASK_REQUEST MaskRequestPacket;
			if ( !SYS::Deserialize(&MaskRequestPacket,Mem) ) {
				auto Response=drogon::HttpResponse::newNotFoundResponse();
				callback(Response);
			}
			else {
				Json::Value jsonRet;
				DB::TEST_RET TestRet = DB::Test(MaskRequestPacket.Id_);
				time_t LastTime = std::chrono::system_clock::to_time_t(TestRet.LastTime_);
				std::tm tmNow = *std::localtime(&LastTime);
				_CHAR Buf[128];
				strftime(Buf, sizeof(Buf), "%A %c", &tmNow);
				_STRING strTime( Buf );
				jsonRet["time"]=strTime;
				if ( TestRet.State_ == DB::TEST_RET::Accept ) {
					jsonRet["result"]="accept";
				}
				else {
					jsonRet["result"]="reject";
				}
				auto Response=drogon::HttpResponse::newHttpJsonResponse(jsonRet);
				callback(Response);
			}
		});
	drogon::app().run();

	DB::Shutdown();
	SYS::Shutdown();
}


