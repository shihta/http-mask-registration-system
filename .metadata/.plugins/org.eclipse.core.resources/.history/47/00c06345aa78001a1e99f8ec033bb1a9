/*
 * Database.cpp
 *
 *  Created on: Apr 6, 2020
 *      Author: bill
 */

#include "./Database.hpp"
#include <Sys/Include/SysDef.hpp>
#include <string.h>
#include <map>
#include <iostream>



namespace DB {

	const _UINT BLOCK_SIZE = 64;
	chrono::seconds DURATION(2*7*24*60*60);
	FILE *FileHandle_=nullptr;
	_UINT numBlocks_=0;;



	struct REC_INDEX {
		_UINT DiskBlockIndex_;
		chrono::steady_clock::time_point LastTime_;
	};
	map<_STRING,REC_INDEX> Index_;



#pragma pack(push,1)
	struct DISK_REC {
		_CHAR Id_[31+1];
		chrono::steady_clock::time_point LastTme_;
	};
#pragma pack(pop)



	void PrepareDiskRec(_BYTE *ptr_block,_STRING const &user_id,chrono::steady_clock::time_point timept) {
		DISK_REC *pDiskRec = (DISK_REC *)ptr_block;
		::strcpy(&(pDiskRec->Id_[0]),user_id.c_str());
		pDiskRec->LastTme_ = timept;
		_BUF Buf(pDiskRec,sizeof(DISK_REC));
		*((_WORD *)(pDiskRec+1)) = SYS::Fletcher16(Buf);
	}



	bool Record_Update(_UINT BlockIndex,_STRING const &id_str,chrono::steady_clock::time_point time_pt) {
		_BYTE Mem[BLOCK_SIZE];
		PrepareDiskRec(Mem,id_str,time_pt);
		long int Pos = BLOCK_SIZE * ((long int)BlockIndex);
		fseek(FileHandle_,Pos,SEEK_SET);
		fwrite(Mem,BLOCK_SIZE,1,FileHandle_);
		return true;
	}



	bool Record_Append(_STRING const &id_str,chrono::steady_clock::time_point time_pt) {
		REC_INDEX RecIndex;
		RecIndex.DiskBlockIndex_ = numBlocks_;
		RecIndex.LastTime_ = time_pt;
		Index_.insert(std::pair<_STRING,REC_INDEX>(id_str,RecIndex) );

		_BYTE Mem[BLOCK_SIZE];
		PrepareDiskRec(Mem,id_str,time_pt);
		long int Pos = BLOCK_SIZE * ((long int)numBlocks_);
		fseek(FileHandle_,Pos,SEEK_SET);
		fwrite(Mem,BLOCK_SIZE,1,FileHandle_);
		++numBlocks_;
		return true;
	}



	TEST_RET Test(_STRING const &id_str) {
		auto Iter = Index_.find(id_str);
		chrono::steady_clock::time_point Now = chrono::steady_clock::now();
		if ( Iter != Index_.end() ) {
			if ( (Now - Iter->second.LastTime_) > DURATION ) {
				cout << "Recycled" << endl;
				_UINT BlockIndex = Iter->second.DiskBlockIndex_;
				Record_Update(BlockIndex,id_str,Now);
				return TEST_RET(TEST_RET::OK);
			}
			else {
				return TEST_RET(TEST_RET::Failed,Iter->second.LastTime_);
			}
		}
		else { // Not found in Index_
			if ( Record_Append(id_str,Now) ) {

				return TEST_RET(TEST_RET::OK);
			}
			else {
				_XASSERT(0);
				return TEST_RET(TEST_RET::Failed);
			}
		}
	}



	bool ReadRecord() {
		_BYTE Mem[BLOCK_SIZE];
		if ( fread(Mem,BLOCK_SIZE,1,FileHandle_) != 1 ) {
			_XASSERT(0);
			return false;
		}
		DISK_REC *pDiskRec = (DISK_REC *)Mem;
		_BUF Buf(pDiskRec,sizeof(DISK_REC));
		_WORD Digest = SYS::Fletcher16(Buf);
		if ( Digest != *((_WORD *)(pDiskRec+1)) ) { // Check digest
			_XASSERT(0);
			return false;
		}
		if ( Index_.find(pDiskRec->Id_) != Index_.end() ) { // Duplicated?
			_XASSERT(0);
			return false;
		}
		REC_INDEX RecIndex;
		RecIndex.DiskBlockIndex_ = numBlocks_;
		RecIndex.LastTime_ = pDiskRec->LastTme_;
		Index_.insert(std::pair<_STRING,REC_INDEX>(pDiskRec->Id_,RecIndex) );
		++numBlocks_;
		return true;
	}



	bool Initialize(filesystem::path const &db_file) {
		cout << "Database: " << db_file.string() << endl;
		char const *StrFileMode;
		if ( !filesystem::exists(db_file) ) {
			StrFileMode = "w+b";
		}
		else {
			StrFileMode = "r+b";
		}
		FileHandle_ = fopen(db_file.string().c_str(),StrFileMode);
		if ( !FileHandle_ ) {
			_XASSERT(0);
			return false;
		}
		numBlocks_ = 0;
		long int FileSize = filesystem::file_size(db_file);
		_UINT countRecs = FileSize / BLOCK_SIZE;
		cout << "Number of Records: " << countRecs << endl;
		fseek(FileHandle_,0,SEEK_SET);
		for (_UINT i=0; i<countRecs;i++) {
			if ( !ReadRecord() ) {
				_XASSERT(0);
				return false;
			}
		}
		return true;
	}



	void Shutdown(){
		fclose(FileHandle_);
	}
};
